class Config:
    """Set Flask configuration vars."""

    # General
    TESTING = True
    FLASK_DEBUG = True
    SECRET_KEY = "4daceae17644d23f45d69e4dcc317d4aaf25d53f"

    # Database
    SQLALCHEMY_DATABASE_URI = "postgresql://getitdone:getitdone@localhost/getitdone"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
