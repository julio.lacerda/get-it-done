import json

from restcountries.api import RestCountries
from six import string_types

from getitdone.api.schema.countries import CountrySchema


class RestCountriesAPI(RestCountries):
    class Meta:
        countries_schema = CountrySchema

    def serialize(self, instances):
        schema = CountrySchema()
        return schema.dumps(instances, many=True)

    def order_by(self, countries, order_by):
        countries = json.loads(countries)
        allowed_columns = ("population", "area", "fetched_at")
        if isinstance(order_by, string_types):
            order_by_list = [x for x in order_by.split(",") if x in allowed_columns]
            return sorted(
                countries, key=lambda x: [x[y] for y in order_by_list], reverse=True
            )
        return countries

    def limit(self, countries, max):
        if isinstance(max, int) or isinstance(max, string_types) and max.isdigit():
            return countries[0 : int(max)]
        return countries
