from datetime import datetime

from marshmallow import fields

from getitdone.api.schema import Schema


class LanguageSchema(Schema):
    name = fields.String(dump_only=True)
    nativeName = fields.String(dump_only=True)
    iso639_1 = fields.String(dump_only=True)
    iso639_2 = fields.String(dump_only=True)


class CurrencySchema(Schema):
    code = fields.String(dump_only=True)
    currency = fields.String(dump_only=True)
    symbol = fields.String(dump_only=True)


class BlocSchema(Schema):
    acronym = fields.String(dump_only=True)
    name = fields.String(dump_only=True)
    otherAcronyms = fields.List(
        fields.String(dump_only=True), attribute="otherAcronyms"
    )
    otherNames = fields.List(fields.String(dump_only=True), attribute="otherNames")


class CountrySchema(Schema):
    name = fields.String(dump_only=True)
    top_level_domain = fields.List(
        fields.String(dump_only=True), attribute="topLevelDomain"
    )
    alpha_2_code = fields.String(dump_only=True, attribute="alpha2Code")
    alpha_3_code = fields.String(dump_only=True, attribute="alpha3Code")
    calling_codes = fields.List(fields.String(dump_only=True), attribute="callingCodes")
    capital = fields.String(dump_only=True)
    alt_spellings = fields.List(fields.String(dump_only=True), attribute="altSpellings")
    region = fields.String(dump_only=True)
    subregion = fields.String(dump_only=True)
    population = fields.Integer(dump_only=True)
    latlng = fields.List(fields.Float(dump_only=True))
    demonym = fields.String(dump_only=True)
    area = fields.Float(dump_only=True)
    gini = fields.Float(dump_only=True)
    timezones = fields.List(fields.String(dump_only=True))
    borders = fields.List(fields.String(dump_only=True))
    currencies = fields.List(fields.Nested(CurrencySchema))
    languages = fields.List(fields.Nested(LanguageSchema))
    native_name = fields.String(dump_only=True, attribute="nativeName")
    numeric_code = fields.String(dump_only=True, attribute="numericCode")
    translations = fields.Dict(dump_only=True)
    flag = fields.String(dump_only=True)
    blocs = fields.List(fields.Nested(BlocSchema))
    fetched_at = fields.DateTime(dump_only=True, default=datetime.now())


class UpdateCountrySchema(Schema):
    area = fields.Float(required=False)
    population = fields.Integer(required=False)
