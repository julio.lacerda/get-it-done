from marshmallow import Schema as MarshmallowSchema
from marshmallow import ValidationError


class Schema(MarshmallowSchema):
    def is_valid(self, data=None):
        try:
            return True, self.load(data)
        except ValidationError as err:
            return False, err
