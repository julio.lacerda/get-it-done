from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.sql import cast


class CastingArray(ARRAY):
    def bind_expression(self, bindvalue):
        return cast(bindvalue, self)
