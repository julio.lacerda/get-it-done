from sqlalchemy.dialects.postgresql import ARRAY, JSONB

from getitdone import db
from getitdone.api.models import CastingArray


class Country(db.Model):
    __tablename__ = "countries"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False)
    top_level_domain = db.Column(ARRAY(db.String(128)))
    alpha_2_code = db.Column(db.String(8), nullable=False)
    alpha_3_code = db.Column(db.String(8), nullable=False, unique=True)
    calling_codes = db.Column(ARRAY(db.String(128)))
    capital = db.Column(db.String(128), nullable=False)
    alt_spellings = db.Column(ARRAY(db.String(128)))
    region = db.Column(db.String(32), nullable=False)
    subregion = db.Column(db.String(32), nullable=False)
    population = db.Column(db.Integer, nullable=True)
    latlng = db.Column(ARRAY(db.Float), nullable=False)
    demonym = db.Column(db.String(64), nullable=False)
    area = db.Column(db.Integer, nullable=True)
    gini = db.Column(db.Float, nullable=True)
    timezones = db.Column(ARRAY(db.String(128)))
    borders = db.Column(ARRAY(db.String(128)))
    currencies = db.Column(CastingArray(JSONB))
    languages = db.Column(CastingArray(JSONB))
    native_name = db.Column(db.String(64), nullable=False)
    numeric_code = db.Column(db.String(64), nullable=True)
    translations = db.Column(JSONB)
    flag = db.Column(db.String(128), nullable=False)
    blocs = db.Column(CastingArray(JSONB))
    fetched_at = db.Column(db.DateTime(), nullable=False)

    def __repr__(self):
        return "<Country %r>" % self.name
