import json
from datetime import datetime

from flask import current_app as app
from flask import jsonify, request
from flask.views import View

from getitdone import db
from getitdone.api.models.countries import Country
from getitdone.api.restcountries import RestCountriesAPI
from getitdone.api.schema.countries import CountrySchema


class CountriesEndpoint(View):
    methods = ["GET"]

    def dispatch_request(self):
        limit = request.args.get("limit", default=None, type=int)
        order_by = request.args.get(
            "order_by", default="population,area,fetched_at", type=str
        )

        restcountries_api = RestCountriesAPI()
        serialized_countries = restcountries_api.limit(
            restcountries_api.order_by(
                restcountries_api.serialize(restcountries_api.region("europe")),
                order_by,
            ),
            limit,
        )

        instances = {}
        for country_data in serialized_countries:
            country_instance = Country(**country_data)
            instances.setdefault(country_instance.alpha_3_code, country_instance)

        for each in Country.query.filter(
            Country.alpha_3_code.in_(instances.keys())
        ).all():
            # Only merge those instances which already exist in the database
            matched_instance = instances.pop(each.alpha_3_code, None)
            if matched_instance:
                matched_instance.id = each.id
                db.session.merge(matched_instance)

        # Only add those instances which did not exist in the database
        db.session.add_all(instances.values())

        # Now we commit our modifications (merges) and inserts (adds) to the database!
        db.session.commit()

        return jsonify(serialized_countries)


app.add_url_rule("/countries", view_func=CountriesEndpoint.as_view("countries"))


class UpdateCountryEndpoint(View):
    methods = ["PUT", "PATCH"]

    class Meta:
        schema = CountrySchema

    def dispatch_request(self, alpha_3_code):
        body = json.loads(request.data)
        schema = self.Meta.schema()
        is_valid, data = schema.is_valid(body)

        if is_valid:
            to_update = {k: v for k, v in data.items() if v is not None}
            to_update.update({"fetched_at": datetime.now()})
            country_update = Country.query.filter_by(
                alpha_3_code=str(alpha_3_code)
            ).update(to_update)
            db.session.commit()
            return jsonify(to_update)
        else:
            response = jsonify(dict(data.messages))
            response.status_code = 400
            return response


app.add_url_rule(
    "/countries/<alpha_3_code>",
    view_func=UpdateCountryEndpoint.as_view("update_country"),
)
