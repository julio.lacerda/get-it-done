"""country

Revision ID: 55e673524596
Revises: d1571dbbaea4
Create Date: 2019-09-05 22:16:21.135355

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects.postgresql import ARRAY, JSONB

# revision identifiers, used by Alembic.
revision = "55e673524596"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "countries",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("name", sa.String(64), nullable=False),
        sa.Column("top_level_domain", ARRAY(sa.String(128)), nullable=False),
        sa.Column("alpha_2_code", sa.String(8), nullable=False),
        sa.Column("alpha_3_code", sa.String(8), nullable=False, unique=True),
        sa.Column("calling_codes", ARRAY(sa.String(128)), nullable=False),
        sa.Column("capital", sa.String(64), nullable=False),
        sa.Column("alt_spellings", ARRAY(sa.String(128)), nullable=False),
        sa.Column("population", sa.Integer, nullable=True),
        sa.Column("region", sa.String(32), nullable=True),
        sa.Column("subregion", sa.String(32), nullable=True),
        sa.Column("latlng", ARRAY(sa.Float), nullable=False),
        sa.Column("demonym", sa.String(64), nullable=False),
        sa.Column("area", sa.Integer, nullable=True),
        sa.Column("gini", sa.Float, nullable=True),
        sa.Column("timezones", ARRAY(sa.String(128)), nullable=False),
        sa.Column("borders", ARRAY(sa.String(128)), nullable=False),
        sa.Column("native_name", sa.String(64), nullable=False),
        sa.Column("numeric_code", sa.String(64), nullable=True),
        sa.Column("currencies", ARRAY(JSONB), nullable=False),
        sa.Column("languages", ARRAY(JSONB), nullable=False),
        sa.Column("translations", JSONB, nullable=True),
        sa.Column("flag", sa.String(128), nullable=False),
        sa.Column("blocs", ARRAY(JSONB), nullable=True),
        sa.Column(
            "fetched_at", sa.DateTime(), nullable=False, server_default=sa.text("now()")
        ),
    )


def downgrade():
    op.drop_table("countries")
