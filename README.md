# Lateral Challenge

OS Requirements:
* python3 (https://www.python.org/downloads/)
* pip (https://pip.pypa.io/en/stable/installing/)
* pipenv (https://docs.pipenv.org/en/latest/install/)

## Installation

Clone the repository
```bash
git clone git@gitlab.com:julio.lacerda/get-it-done.git
```

Open the directory
```bash
cd get-it-done
```

Install all the application requirements
```bash
pipenv install
```

Activate pipenv environment
```bash
pipenv shell
```

Initialize the service
```bash
python -m flask run
```

Exit the pipenv environment
```bash
exit
```
